# Backend task

Make a small Node.js application, use apollo-server for GraphQL implementation.
API supports only one query.

Assume we have the list of customers (Name, Surname, E-mail) together with the list of therapists (Name, Surname, Seniority - Junior / Senior). Every customer has one therapist. 

Build a schema. The query returns a list of customers with the assigned therapist. It supports filter by therapist's seniority (example: get all customers with the assigned senior therapist).

## Requirements
 - Use Node.js with Typescipt
 - Make playground visible
 - Tests
 
## How to do it:
 - clone this repository
 - make a merge request for the review
 - use mock data
